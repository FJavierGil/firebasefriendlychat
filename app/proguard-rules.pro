# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in ~/android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# When using Firebase Realtime Database in your app along with ProGuard you need
# to consider how your model objects will be serialized and deserialized after obfuscation.
# If you use DataSnapshot.getValue(Class) or DatabaseReference.setValue(Object) to read and
# write data you will need to add rules to the proguard-rules.pro file:
-keepattributes Signature

# This rule will properly ProGuard all the model classes in
# the package com.yourcompany.models. Modify to fit the structure
# of your app.
-keepclassmembers class com.google.firebase.udacity.friendlychat.** {
  *;
}